$(document).ready(function() {
    $('.gentle1').gentleSelect();
    $('.gentle2').gentleSelect();
    $('.gentle3').gentleSelect();
    $('.gentle4').gentleSelect();

    // City Page Appointment form
    $('.gentle5').gentleSelect();
    $('.gentle6').gentleSelect();
    $('.gentle7').gentleSelect();
    $('.gentle8').gentleSelect();
    $('.gentle9').gentleSelect();
    $('.gentle10').gentleSelect();
});

$('ul.gentleselect-dialog ul li').each(function(){
    if($(this).hasClass('selected')) {
        $(this).closest('.select-parent').removeClass("disabled");
    }
});
